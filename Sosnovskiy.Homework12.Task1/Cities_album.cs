﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.Homework12.Task1
{
    public partial class citiesAlbum : Form
    {
        // List of radiobuttons
        private List<RadioButton> _radioButtons;

        // Iterator variable
        private int _iterator;

        // Random variable
        Random random;

        public citiesAlbum()
        {
            InitializeComponent();

            // Setting list of radiobuttons
            _radioButtons = new List<RadioButton>();
            foreach (RadioButton item in gbWorldPart.Controls)
            {
                _radioButtons.Add(item);
            }

            random = new Random();
        }

        private async void citiesAlbum_Load(object sender, EventArgs e)
        {
            // Setting checked on random radiobutton
            _radioButtons[random.Next(0, 5)].Checked = true;

            // Setting text blink on form title for 3s
            Timer textBlinking = new Timer();
            textBlinking.Interval = 250;
            textBlinking.Tick += new EventHandler(timer_Tick);
            textBlinking.Start();

            await Task.Delay(3000);

            textBlinking.Dispose();

            this.Text = "Cities album";
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string title = "Select world part and city";

            this.Text = this.Text.Equals(title) ? "" : title;
        }

        private void rb_CheckedChanged(object sender, EventArgs e)
        {
            // Get sender radioButton and if checked - set images
            RadioButton radioButton = (RadioButton)sender;

            if (radioButton.Checked)
            {
                setCityImages(radioButton.Text);
            }
        }

        private void cbCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            citiesIterator();
        }

        private void setCityImages(string country)
        {
            // Set images with city-image dependencies
            CitiesGenerator.Country = country;
            CitiesGenerator.SetDependencies(imageList);
            cbCities.DataSource = CitiesGenerator.CitiesInBox;

            cbCities.SelectedIndex = random.Next(0, 2);

            citiesIterator();
        }

        private void citiesIterator()
        {
            _iterator = 0;

            foreach (KeyValuePair<string, Image> item in CitiesGenerator.CitiesList)
            {
                if (item.Key.Equals(cbCities.GetItemText(cbCities.SelectedItem)))
                {
                    pbCities.Image = item.Value;
                }
                _iterator++;
            }
        }
    }
}