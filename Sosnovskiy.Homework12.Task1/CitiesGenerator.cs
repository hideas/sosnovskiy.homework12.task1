﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.Homework12.Task1
{
    /// <summary>
    ///  Static class for generating city-image dependencies and cities list for Combobox
    /// </summary>
    static class CitiesGenerator
    {
        /// <summary>
        ///  Property for setting and getting country
        /// </summary>
        public static string Country { get; set; }

        // Private property with city-image dependencies
        public static Dictionary<string, Image> CitiesList { get; private set; } = new Dictionary<string, Image>();

        /// <summary>
        ///  List of cities in specific world part for sending to Combobox
        /// </summary>
        public static List<string> CitiesInBox { get; private set; }

        /// <summary>
        ///  Method for setting city-image dependencies with specific images collection
        /// </summary>
        public static void SetCities(ImageList imageList)
        {
            CitiesList.Add("Adelaide", imageList.Images[0]);
            CitiesList.Add("Barcelona", imageList.Images[1]);
            CitiesList.Add("Buenos Aires", imageList.Images[2]);
            CitiesList.Add("Cairo", imageList.Images[3]);
            CitiesList.Add("Johannesburg", imageList.Images[4]);
            CitiesList.Add("Paris", imageList.Images[5]);
            CitiesList.Add("Rio de Janeiro", imageList.Images[6]);
            CitiesList.Add("San Francisko", imageList.Images[7]);
            CitiesList.Add("Singapore", imageList.Images[8]);
            CitiesList.Add("Sydney", imageList.Images[9]);
            CitiesList.Add("Tokio", imageList.Images[10]);
            CitiesList.Add("Toronto", imageList.Images[11]);
        }

        /// <summary>
        ///  Method for setting city-image dependencies and list for Combobox
        /// </summary>
        public static void SetDependencies(ImageList imageList)
        {
            if (CitiesList.Count == 0)
            {
                SetCities(imageList);
            }

            CitiesInBox = new List<string>();
            if (Country.Equals("Europe"))
            {
                CitiesInBox.Add("Barcelona");
                CitiesInBox.Add("Paris");
            }
            if (Country.Equals("Asia"))
            {
                CitiesInBox.Add("Singapore");
                CitiesInBox.Add("Tokio");
            }
            if (Country.Equals("Africa"))
            {
                CitiesInBox.Add("Cairo");
                CitiesInBox.Add("Johannesburg");
            }
            if (Country.Equals("Northern America"))
            {
                CitiesInBox.Add("Toronto");
                CitiesInBox.Add("San Francisko");
            }
            if (Country.Equals("Southern America"))
            {
                CitiesInBox.Add("Buenos Aires");
                CitiesInBox.Add("Rio de Janeiro");
            }
            if (Country.Equals("Australia"))
            {
                CitiesInBox.Add("Adelaide");
                CitiesInBox.Add("Sydney");
            }
        }
    }
}
