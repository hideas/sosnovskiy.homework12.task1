﻿namespace Sosnovskiy.Homework12.Task1
{
    partial class citiesAlbum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(citiesAlbum));
            this.gbWorldPart = new System.Windows.Forms.GroupBox();
            this.rbAustralia = new System.Windows.Forms.RadioButton();
            this.rbSouthernAmerica = new System.Windows.Forms.RadioButton();
            this.rbNorthernAmerica = new System.Windows.Forms.RadioButton();
            this.rbAfrica = new System.Windows.Forms.RadioButton();
            this.rbAsia = new System.Windows.Forms.RadioButton();
            this.rbEurope = new System.Windows.Forms.RadioButton();
            this.gbCities = new System.Windows.Forms.GroupBox();
            this.pbCities = new System.Windows.Forms.PictureBox();
            this.cbCities = new System.Windows.Forms.ComboBox();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.gbWorldPart.SuspendLayout();
            this.gbCities.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCities)).BeginInit();
            this.SuspendLayout();
            // 
            // gbWorldPart
            // 
            this.gbWorldPart.Controls.Add(this.rbAustralia);
            this.gbWorldPart.Controls.Add(this.rbSouthernAmerica);
            this.gbWorldPart.Controls.Add(this.rbNorthernAmerica);
            this.gbWorldPart.Controls.Add(this.rbAfrica);
            this.gbWorldPart.Controls.Add(this.rbAsia);
            this.gbWorldPart.Controls.Add(this.rbEurope);
            this.gbWorldPart.Location = new System.Drawing.Point(12, 12);
            this.gbWorldPart.Name = "gbWorldPart";
            this.gbWorldPart.Size = new System.Drawing.Size(150, 166);
            this.gbWorldPart.TabIndex = 0;
            this.gbWorldPart.TabStop = false;
            this.gbWorldPart.Text = "World part";
            // 
            // rbAustralia
            // 
            this.rbAustralia.AutoSize = true;
            this.rbAustralia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbAustralia.Location = new System.Drawing.Point(19, 134);
            this.rbAustralia.Name = "rbAustralia";
            this.rbAustralia.Size = new System.Drawing.Size(65, 17);
            this.rbAustralia.TabIndex = 6;
            this.rbAustralia.Text = "Australia";
            this.rbAustralia.UseVisualStyleBackColor = true;
            this.rbAustralia.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rbSouthernAmerica
            // 
            this.rbSouthernAmerica.AutoSize = true;
            this.rbSouthernAmerica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbSouthernAmerica.Location = new System.Drawing.Point(19, 111);
            this.rbSouthernAmerica.Name = "rbSouthernAmerica";
            this.rbSouthernAmerica.Size = new System.Drawing.Size(109, 17);
            this.rbSouthernAmerica.TabIndex = 5;
            this.rbSouthernAmerica.Text = "Southern America";
            this.rbSouthernAmerica.UseVisualStyleBackColor = true;
            this.rbSouthernAmerica.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rbNorthernAmerica
            // 
            this.rbNorthernAmerica.AutoSize = true;
            this.rbNorthernAmerica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbNorthernAmerica.Location = new System.Drawing.Point(19, 88);
            this.rbNorthernAmerica.Name = "rbNorthernAmerica";
            this.rbNorthernAmerica.Size = new System.Drawing.Size(107, 17);
            this.rbNorthernAmerica.TabIndex = 4;
            this.rbNorthernAmerica.Text = "Northern America";
            this.rbNorthernAmerica.UseVisualStyleBackColor = true;
            this.rbNorthernAmerica.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rbAfrica
            // 
            this.rbAfrica.AutoSize = true;
            this.rbAfrica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbAfrica.Location = new System.Drawing.Point(19, 65);
            this.rbAfrica.Name = "rbAfrica";
            this.rbAfrica.Size = new System.Drawing.Size(52, 17);
            this.rbAfrica.TabIndex = 3;
            this.rbAfrica.Text = "Africa";
            this.rbAfrica.UseVisualStyleBackColor = true;
            this.rbAfrica.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rbAsia
            // 
            this.rbAsia.AutoSize = true;
            this.rbAsia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbAsia.Location = new System.Drawing.Point(19, 42);
            this.rbAsia.Name = "rbAsia";
            this.rbAsia.Size = new System.Drawing.Size(45, 17);
            this.rbAsia.TabIndex = 2;
            this.rbAsia.Text = "Asia";
            this.rbAsia.UseVisualStyleBackColor = true;
            this.rbAsia.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rbEurope
            // 
            this.rbEurope.AutoSize = true;
            this.rbEurope.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbEurope.Location = new System.Drawing.Point(19, 19);
            this.rbEurope.Name = "rbEurope";
            this.rbEurope.Size = new System.Drawing.Size(59, 17);
            this.rbEurope.TabIndex = 1;
            this.rbEurope.Text = "Europe";
            this.rbEurope.UseVisualStyleBackColor = true;
            this.rbEurope.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // gbCities
            // 
            this.gbCities.Controls.Add(this.pbCities);
            this.gbCities.Controls.Add(this.cbCities);
            this.gbCities.Location = new System.Drawing.Point(175, 12);
            this.gbCities.Name = "gbCities";
            this.gbCities.Size = new System.Drawing.Size(200, 166);
            this.gbCities.TabIndex = 7;
            this.gbCities.TabStop = false;
            this.gbCities.Text = "Cities";
            // 
            // pbCities
            // 
            this.pbCities.InitialImage = null;
            this.pbCities.Location = new System.Drawing.Point(15, 56);
            this.pbCities.Name = "pbCities";
            this.pbCities.Size = new System.Drawing.Size(169, 95);
            this.pbCities.TabIndex = 8;
            this.pbCities.TabStop = false;
            // 
            // cbCities
            // 
            this.cbCities.DropDownHeight = 42;
            this.cbCities.FormattingEnabled = true;
            this.cbCities.IntegralHeight = false;
            this.cbCities.Location = new System.Drawing.Point(15, 16);
            this.cbCities.MaxDropDownItems = 3;
            this.cbCities.Name = "cbCities";
            this.cbCities.Size = new System.Drawing.Size(169, 21);
            this.cbCities.TabIndex = 8;
            this.cbCities.SelectedIndexChanged += new System.EventHandler(this.cbCities_SelectedIndexChanged);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Adelaide.jpg");
            this.imageList.Images.SetKeyName(1, "Barcelona.jpg");
            this.imageList.Images.SetKeyName(2, "Buenos Aires.jpg");
            this.imageList.Images.SetKeyName(3, "Cairo.jpg");
            this.imageList.Images.SetKeyName(4, "Johannesburg.jpg");
            this.imageList.Images.SetKeyName(5, "Paris.jpg");
            this.imageList.Images.SetKeyName(6, "Rio de Janeiro.jpg");
            this.imageList.Images.SetKeyName(7, "San Francisko.jpg");
            this.imageList.Images.SetKeyName(8, "Singapore.jpg");
            this.imageList.Images.SetKeyName(9, "Sydney.jpg");
            this.imageList.Images.SetKeyName(10, "Tokio.jpg");
            this.imageList.Images.SetKeyName(11, "Toronto.jpg");
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // citiesAlbum
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(387, 188);
            this.Controls.Add(this.gbCities);
            this.Controls.Add(this.gbWorldPart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "citiesAlbum";
            this.Text = "Cities album";
            this.Load += new System.EventHandler(this.citiesAlbum_Load);
            this.gbWorldPart.ResumeLayout(false);
            this.gbWorldPart.PerformLayout();
            this.gbCities.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbCities)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbWorldPart;
        private System.Windows.Forms.GroupBox gbCities;
        private System.Windows.Forms.RadioButton rbEurope;
        private System.Windows.Forms.RadioButton rbAsia;
        private System.Windows.Forms.RadioButton rbNorthernAmerica;
        private System.Windows.Forms.RadioButton rbAfrica;
        private System.Windows.Forms.RadioButton rbAustralia;
        private System.Windows.Forms.RadioButton rbSouthernAmerica;
        private System.Windows.Forms.ComboBox cbCities;
        private System.Windows.Forms.PictureBox pbCities;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Timer timer;
    }
}

